import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
  NavLink
} from "react-router-dom"
import LoginPage from './Components/Pages/LoginPage';
import ProfilePage from './Components/Pages/ProfilePage';
import TranslationPage from './Components/Pages/TranslationPage';

function App() {

  return (
  <BrowserRouter>   
    <div className='App'>
      <nav>
        <li><NavLink to="/login">Login</NavLink></li>
        <li><NavLink to="/translation">Profile</NavLink></li>
        <li><NavLink to="/profile">Translation</NavLink></li>
      </nav>
      <Routes>
        <Route path="/login" element={<LoginPage/>} />
        <Route path="/translation" element={<ProfilePage/>} />
        <Route path="/profile" element={<TranslationPage/>} />
        {/* <Route path="*" component={NotFound} /> */}
      </Routes>
    </div>
  </BrowserRouter>
  );
}

export default App;
